""" Crop functions for image processing. """

import cv2
import numpy as np


def crop(img, x0=None, y0=None, x1=None, y1=None):
    """Crop img[y0:y1, x0:x1] from img and return this part.

    Args:
        img (numpy.ndarray): an RGB or grayscale image. For RGB image,  shape
        is [h, w, c]. for grayscale image, shape is [h, w]
        y0 (int): top y coordinates of target part of img to crop
        y1 (int): bottom y coordinates of target part of img to crop
        x0 (int): left x coordinates of target part of img to crop
        x1 (int): right x coordinates of target part of img to crop


    Returns:
        img[y0:y1, x0:x1].
    Raises:
    ValueError: if value of coordinates < 0
    """

    assert isinstance(img, np.ndarray), 'img is not a np.ndarray'
    assert img.ndim in [2, 3], 'img must be RGB image or grayscale image'
    assert isinstance(y0, int), 'y0 is not a int'
    assert isinstance(y1, int), 'y1 is not a int'
    assert isinstance(x0, int), 'x0 is not a int'
    assert isinstance(x1, int), 'x1 is not a int'

    if y0 < 0:
        raise ValueError('y0 is < 0')
    if y1 < 0:
        raise ValueError('y1 is < 0')
    if x0 < 0:
        raise ValueError('x0 is < 0')
    if x1 < 0:
        raise ValueError('x1 is < 0')

    h, w = img.shape[:2]
    y0 = 0 if y0 is None else y0
    y1 = h if y1 is None else y1
    x0 = 0 if x0 is None else x0
    x1 = w if x1 is None else x1

    return img[y0:y1, x0:x1]

yammtool.image.crop
===================================
.. automodule:: yammtool.image.crop
.. currentmodule:: yammtool.image.crop
.. autofunction:: crop
